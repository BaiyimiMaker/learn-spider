package com.example.spider.common.handler;

import com.example.spider.common.task.SpiderTask;

/**
 * 爬虫结果处理器
 * @author lym
 */
public interface SpiderResultHandler {

    /**
     * 返回能否处理
     */
    boolean canHandle(SpiderTask task, String result);

    /**
     * 处理结果
     * @param result 页面信息
     */
    void handle(SpiderTask task, String result);

}
