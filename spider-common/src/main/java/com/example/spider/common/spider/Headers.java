package com.example.spider.common.spider;

import lombok.Data;

import java.util.Map;

/**
 * Headers
 *
 * @author lym
 */
@Data
public class Headers {

    Map<String, String> headers;

}
