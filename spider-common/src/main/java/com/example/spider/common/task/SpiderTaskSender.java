package com.example.spider.common.task;

/**
 * 负责投递 spiderTask
 * @author lym
 */
public interface SpiderTaskSender {

    /**
     * 将 spiderTask 发送出去，保证让spider能感知到
     * @param spiderTask 爬虫任务
     */
    void send(SpiderTask spiderTask);

}
