package com.example.spider.common.task;

import java.util.Map;

/**
 * 获取 cctv 新闻列表任务
 * @author lym
 */
public abstract class AbstractSpiderTask implements SpiderTask {

    private String url;

    protected Map<String, Object> extendMap;

    @Override
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
