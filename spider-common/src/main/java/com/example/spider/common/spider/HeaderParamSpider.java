package com.example.spider.common.spider;

import cn.hutool.http.HttpRequest;
import com.example.spider.common.task.SpiderTask;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 带固定请求头的爬虫
 * @author lym
 */
@Slf4j
public class HeaderParamSpider implements Spider {

    @Override
    public String doSpider(SpiderTask task) {
        String method = task.getMethod();
        Map<String, String> headers = task.getHeader();
        Map<String, Object> params = task.getParam();
        HttpRequest request = "GET".equals(method) ? HttpRequest.get(task.getUrl()) : HttpRequest.post(task.getUrl());
        headers.forEach(request::header);
        params.forEach(request::form);
        log.info("starting spider To " + task.getUrl());
        String result = request.execute().body();
        log.info("finished spider To " + task.getUrl());
        return result;
    }

}
