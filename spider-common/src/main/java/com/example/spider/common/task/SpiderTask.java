package com.example.spider.common.task;

import java.util.Collections;
import java.util.Map;

/**
 * 爬虫任务
 * @author lym
 */
public interface SpiderTask {

    /**
     * 爬虫任务类别，根据此判断处理器等
     */
    String getType();

    /**
     * 爬取地址
     */
    String getUrl();


    default String getMethod(){
        return "GET";
    }

    /**
     * 扩展使用
     */
    default Map<String, String> getHeader(){
        return Collections.emptyMap();
    }

    /**
     * 扩展使用
     */
    default Map<String, Object> getParam(){
        return Collections.emptyMap();
    }

}
