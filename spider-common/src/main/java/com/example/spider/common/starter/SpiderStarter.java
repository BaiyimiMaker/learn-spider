package com.example.spider.common.starter;

/**
 * 爬虫启动触发器
 * @author lym
 */
public interface SpiderStarter {

    /**
     * 启动爬虫任务
     */
    void startSpiders();
}
