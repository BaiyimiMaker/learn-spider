package com.example.spider.common.spider;

import com.example.spider.common.task.SpiderTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

/**
 * 使用restTemplate 的爬虫
 * @author lym
 */
@Slf4j
public class RestSpider implements Spider {

    private final RestTemplate rest;

    public RestSpider(RestTemplate rest) {
        this.rest = rest;
    }

    @Override
    public String doSpider(SpiderTask task) {
        log.info("starting spider To " + task.getUrl());
        String url = task.getUrl();
        String result = rest.getForObject(url, String.class);
        log.info("finished spider To " + task.getUrl());
        return result;
    }




}
