package com.example.spider.common.config;

import com.example.spider.common.handler.SpiderResultHandler;
import com.example.spider.common.handler.SpiderResultHandlerManager;
import com.example.spider.common.util.MyThreadFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author lym
 */
@Configuration
public class SpiderResultHandlerAutoConfiguration {

    public static final String HANDLER = "spiderResultHandlerThreadPool";

    @Bean
    public SpiderResultHandlerManager spiderResultHandlerManager(List<SpiderResultHandler> resultHandlers,
                       @Qualifier(HANDLER) ThreadPoolExecutor executor) {
        return new SpiderResultHandlerManager(resultHandlers, executor);
    }

    @Bean(HANDLER)
    @Qualifier(HANDLER)
    public ThreadPoolExecutor handlerThreadPool(){
        return new ThreadPoolExecutor(5, 5, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingDeque<>(2000), new MyThreadFactory("HANDLER", true));
    }

}
