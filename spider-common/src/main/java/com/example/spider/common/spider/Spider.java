package com.example.spider.common.spider;

import com.example.spider.common.task.SpiderTask;

/**
 * 爬虫
 * @author lym
 */
public interface Spider {

    /**
     * 进行爬虫
     */
    String doSpider(SpiderTask task);

}
