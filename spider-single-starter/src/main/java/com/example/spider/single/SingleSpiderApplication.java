package com.example.spider.single;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 单机版
 * @author lym
 */
@EnableScheduling
@SpringBootApplication
public class SingleSpiderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SingleSpiderApplication.class, args);
    }

}
