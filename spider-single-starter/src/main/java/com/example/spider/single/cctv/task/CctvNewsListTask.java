package com.example.spider.single.cctv.task;

import com.example.spider.common.task.SpiderTask;
import lombok.Builder;
import lombok.Data;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * 获取 cctv 新闻列表任务
 * @author lym
 */
@Data
@Builder
public class CctvNewsListTask implements SpiderTask {

    private String url;

    private Pattern pattern;

    private Map<String, Object> param;

    private int page;

    @Override
    public String getType() {
        return "cctvNewsList";
    }

}
