package com.example.spider.single.queue;

import com.example.spider.common.task.SpiderTask;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * 任务队列
 * @author lym
 */
public class SpiderTaskQueue {

    private static BlockingDeque<SpiderTask> blockingDeque = new LinkedBlockingDeque<>(10_000);

    public static boolean putTask(@NonNull SpiderTask task){
        return blockingDeque.offer(task);
    }

    @Nullable
    public static SpiderTask getTask(){
        return blockingDeque.poll();
    }

    public static int getSize(){
        return blockingDeque.size();
    }


}
