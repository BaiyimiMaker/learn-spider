package com.example.spider.single.cctv.dto;

import lombok.Data;

import java.util.List;

/**
 * @author lym
 */
@Data
public class CctvNewsList {
    private int total;
    private List<CctvNewsItem> list;
}