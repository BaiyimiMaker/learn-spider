package com.example.spider.single.schedule;

import com.example.spider.common.config.SpiderAutoConfiguration;
import com.example.spider.common.handler.SpiderResultHandlerManager;
import com.example.spider.common.spider.Spider;
import com.example.spider.common.starter.SpiderStarter;
import com.example.spider.common.task.SpiderTask;
import com.example.spider.single.queue.SpiderTaskQueue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 爬虫启动器
 * @author lym
 */
@Slf4j
@Component
public class SpiderScheduled {

    @Autowired
    private Spider spider;

    @Autowired
    private SpiderResultHandlerManager resultHandlerManager;

    @Autowired(required = false)
    private List<SpiderStarter> spiderStarters;

    @Autowired
    @Qualifier(SpiderAutoConfiguration.SPIDER)
    private ThreadPoolExecutor executor;

    private static final String PER_5S = "*/5 * * * * ?";

    /**
     * 每分钟执行
     */
    private static final String PER_1MIN = "0 * * * * ?";

    /**
     * 每天凌晨2点执行一次
     */
    private static final String WHEN_2_AM = "0 0 2 * * ?";

    /**
     * 每 5s 从任务队列中拿出所有任务放到队列
     */
    @Scheduled(cron = PER_5S)
    public void starSpider() {
        SpiderTask task = SpiderTaskQueue.getTask();
        if(task == null){
            log.debug("try getTask from queue, but nothing.");
        }else {
            log.debug("getTask success, num of surplus tasks:{}", SpiderTaskQueue.getSize());
        }
        while (task != null) {
            doSpider(task);
            task = SpiderTaskQueue.getTask();
        }
    }

    private void doSpider(SpiderTask task) {
        log.info("creat new SpiderTask({})", task.getUrl());
        executor.execute(() -> {
            resultHandlerManager.handle(task, spider.doSpider(task));
        });
    }

    /**
     * 每天凌晨2点启动所有爬虫任务
     */
    @Scheduled(cron = WHEN_2_AM)
    public void startAllSpiders(){
        if(!CollectionUtils.isEmpty(spiderStarters)){
            spiderStarters.forEach(SpiderStarter::startSpiders);
        }
    }

}
