package com.example.spider.single.cctv;

import com.example.spider.common.starter.SpiderStarter;
import com.example.spider.single.queue.SpiderTaskQueue;
import org.springframework.stereotype.Component;

/**
 * @author lym
 */
@Component
public class CctvNewsSpiderStarter implements SpiderStarter {

    @Override
    public void startSpiders(){
        SpiderTaskQueue.putTask(CctvNewsListTaskFactory.createTask(1));
    }

}
