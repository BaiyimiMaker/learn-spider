# learn-spider

#### Description
学习Java爬虫，并总结一个简单的框架

#### Software Architecture
Software architecture description

#### Installation

1.  git clone https://gitee.com/ChinaLym/learn-spider
2.  just run it

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


### 功能



- 迷你的爬虫框架
    - 分布式部署 √
    - URL 去重：布隆过滤器 √
    - 抓取 √
    - 解析任务 √
    - 流程控制 √ 依赖任务队列
    - 任务调度 √ 依赖任务队列
    - 清洗    √
    - 解析    √
    - 字符编码自动识别 √
    - 资源存储：redis、mysql、mongo 自行扩展
    - 代理服务器（静态、动态） ×
    - 状态控制 ×
    - 资源清理 ×

```
OkHttp请求指定url，返回了一个GBK编码的网页字节流；
OkHttp以默认UTF-8进行解码（此时已乱），并以UTF-16方式编码为Java的String类型，返回给处理程序。（为什么以UTF-16方式编码？因为Java的数据在内存中的编码是UTF-16）；
爬虫拿到这个编码错误的String类型的网页，调用MongoDB的API，将数据编码为UTF-8存储到数据库中。所以最后在数据库看到的数据是乱的。
```

1. Http协议的响应头中的约定：

　　Content-Type: text/html;charset=utf-8

2. Html中meta标签中的约定：

　　<meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8“/>

https://www.cnblogs.com/tuohai666/category/1192541.html

探测字符编码 Mozilla Firefox包含有一个自动检测字符编码的库，已经移植到Python中，叫做chardet。