# learn-spider

## 介绍
学习Java爬虫，并总结一个简单的框架

## 架构思想介绍

- 把爬虫流程化，定义如下
    - 发起 http 请求，并返回响应结果
    - 处理（解析响应内容，保存等）
- 把这一个完整的流程定义为一个爬虫任务。 SpiderTask

- 那么进行一次爬虫就可以理解为，
    - 创建爬虫任务（SpiderTask），放到爬虫队列（SpiderTaskQueue）
    - 爬虫调度器从爬虫队列中取出任务交给爬虫（ Spider ）并返回响应结果（Result）
    - 交给结果处理器（ResultHandler）处理这个 Result 完成这个任务
    
- 其中，爬虫爬取和处理器处理较为耗时，使用多线程进行，详细流程参见如图

![爬虫流程](spider.png)    

## 实现功能

爬取某新闻网站，并将其保存到文件中，以供后续文字处理和神经网络模型的训练

启动时触发一次全量爬取，每天凌晨2点自动更新

在 test 包中，通过函数式编程实现爬取功能（10行代码左右）

在 java 包中，以面向对象的思想实现，并抽象出通用爬虫框架

## 使用说明

1.  `git clone https://gitee.com/ChinaLym/learn-spider`
2.  本地运行

## 分支介绍

- single 单体架构
- cluster 集群/分布式架构，分为spider、handler两个应用，集群部署。每个spider 或 handler 都可以动态扩容或缩容。

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
