# spider-cluster

多实例部署的爬虫默认依赖了 RabbitMQ 实现交互和通信

### 集群版本的爬虫 demo
- spider-cluster-starter

### 分布式版本的爬虫demo
- spider-distribute-spider-starter
- spider-distribute-handler-starter

- 为什么要将 spider，handler 分开？
    - 考虑到有这样的场景：有多台性能差的机器，少量性能好的机器，将 spider 部署到 性能差的机器上，将 MQ，handler 部署到性能好的机器上，可以充分利用网络。
    - 异构：某一部分由其他语言实现。如 spider 部分可能更适合用python实现。或者handler部分由非Java实现（比如C++）。
    - 实际中，如果多台机器性能差不多，可以将 spider 和 handler 放一个服务中运行，更节省资源，一致性问题也更好解决