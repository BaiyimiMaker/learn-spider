package com.example.spider.cluster.common.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * SpiderResultHandler 需要的通道
 *  获取爬虫结果
 *  发送爬虫任务
 * @author lym
 */
public interface HandlerChannel {

    /**
     * 获取 spiderTask 的 channel
     */
    @Input(SpiderQueueDefinition.SPIDER_RESULT_IN)
    SubscribableChannel getResult();

    /**
     * 输出 spiderTask 的 channel
     */
    @Output(SpiderQueueDefinition.SPIDER_TASK_OUT)
    MessageChannel putTasl();
}
