package com.example.spider.cluster.common.stream;

/**
 * 定义
 * @author lym
 */
public interface SpiderQueueDefinition {

    String SPIDER_TASK_IN = "spider_task_get";

    String SPIDER_TASK_OUT = "spider_task_put";


    String SPIDER_RESULT_IN = "spider_result_get";

    String SPIDER_RESULT_OUT = "spider_result_put";


}
