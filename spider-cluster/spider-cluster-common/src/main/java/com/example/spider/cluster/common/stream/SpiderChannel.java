package com.example.spider.cluster.common.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * 爬虫 channel
 *  收爬虫任务，发爬取结果
 * @author lym
 */
public interface SpiderChannel {


    /**
     * 获取 spiderTask 的 channel
     */
    @Input(SpiderQueueDefinition.SPIDER_TASK_IN)
    SubscribableChannel getTask();

    /**
     * 输出 spiderResult 的 channel
     */
    @Output(SpiderQueueDefinition.SPIDER_RESULT_OUT)
    MessageChannel putesult();
}
