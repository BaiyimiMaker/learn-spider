package com.example.spider.cluster.common.dto;

import lombok.Data;

import java.util.UUID;

/**
 * mq 消息统一包装类
 * @author lym
 */
@Data
public class StandMqMessage {

    /**
     * 消息类型
     */
    private String type;

    /**
     * 消息唯一标识
     */
    private String id = UUID.randomUUID().toString().replace("-", "");

    /**
     * 哪个服务发出的
     */
    private String from;

    /**
     * 真正的 msg json 化的部分
     */
    private String data;

}