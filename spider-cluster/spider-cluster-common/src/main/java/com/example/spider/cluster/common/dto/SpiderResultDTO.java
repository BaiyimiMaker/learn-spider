package com.example.spider.cluster.common.dto;

import com.example.spider.cluster.common.task.SpiderTaskTypeManager;
import com.example.spider.common.task.SpiderTask;
import com.example.spider.common.util.JsonUtils;
import lombok.Data;

/**
 * 爬虫结果
 * @author lym
 */
@Data
public class SpiderResultDTO<T extends SpiderTask> {

    private T spiderTask;

    private String result;

    public static <TYPE extends SpiderTask> SpiderResultDTO<TYPE> fromMq(StandMqMessage standMqMessage){
        return SpiderTaskTypeManager.toSpiderResultDTO(standMqMessage.getData(), standMqMessage.getType());
    }

    public StandMqMessage toStandMqMessage(){
        StandMqMessage standMqMessage = new StandMqMessage();
        standMqMessage.setData(JsonUtils.toJson(this));
        standMqMessage.setType(spiderTask.getType());
        return standMqMessage;
    }

}
