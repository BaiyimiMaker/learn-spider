package com.example.spider.cluster.common;

import com.example.spider.cluster.common.task.CctvNewsDetailTask;
import com.example.spider.cluster.common.task.CctvNewsListTask;
import com.example.spider.cluster.common.task.SpiderTaskTypeManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author lym
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
public class SpiderTaskTypeListener implements ApplicationListener<ContextRefreshedEvent> {

    @Value("${spider.task.package:com.example.spider.cluster.common.task}")
    private String spiderTaskPackage;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        SpiderTaskTypeManager.addToSpiderMap(Arrays.asList(CctvNewsDetailTask.class, CctvNewsListTask.class));
        //SpiderTaskTypeManager.scanSpiderTaskPackage(spiderTaskPackage);
    }
}
