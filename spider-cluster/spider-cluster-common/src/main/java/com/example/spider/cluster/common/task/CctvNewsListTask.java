package com.example.spider.cluster.common.task;

import com.example.spider.common.task.SpiderTask;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * 获取 cctv 新闻列表任务
 * @author lym
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CctvNewsListTask implements SpiderTask {

    private String url;

    private Pattern pattern;

    private Map<String, String> param;

    private int page;

    @Override
    public String getType() {
        return "cctvNewsList";
    }

}
