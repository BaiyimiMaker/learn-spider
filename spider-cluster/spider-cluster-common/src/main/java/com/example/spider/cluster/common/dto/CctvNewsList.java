package com.example.spider.cluster.common.dto;

import lombok.Data;

import java.util.List;

/**
 * @author lym
 */
@Data
public class CctvNewsList {
    private int total;
    private List<CctvNewsItem> list;
}