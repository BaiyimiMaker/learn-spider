package com.example.spider.cluster.common.task;

import com.example.spider.cluster.common.dto.CctvNewsItem;
import com.example.spider.common.task.SpiderTask;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * 获取 cctv 新闻详情任务
 * @author lym
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CctvNewsDetailTask implements SpiderTask {

    private String url;

    private Pattern pattern;

    private Map<String, String> param;

    private CctvNewsItem item;

    @Override
    public String getType() {
        return "cctvNewsDetail";
    }
}
