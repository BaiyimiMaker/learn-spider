package com.example.spider.cluster.handler.cctv.start;


import com.example.spider.cluster.common.task.CctvNewsListTask;

/**
 * 新浪新闻爬虫任务生成器
 * @author lym
 */
public class CctvNewsListTaskFactory {

    public static CctvNewsListTask createTask(int pageNo) {
        String urlTemplate = "http://news.cctv.com/2019/07/gaiban/cmsdatainterface/page/news_%d.jsonp?cb=t&cb=news";
        CctvNewsListTask task = new CctvNewsListTask();
        task.setUrl(String.format(urlTemplate, pageNo));
        task.setPage(pageNo);
        return task;
    }

}
