package com.example.spider.cluster.handler.cctv.start;

import com.example.spider.common.starter.SpiderStarter;
import com.example.spider.common.task.SpiderTaskSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author lym
 */
@Slf4j
@Component
public class CctvNewsStarter implements SpiderStarter {

    @Autowired
    private SpiderTaskSender spiderTaskSender;

    @Override
    public void startSpiders() {
        log.info("cctvNewsSpider starting!");
        spiderTaskSender.send(CctvNewsListTaskFactory.createTask(1));
        log.info("cctvNewsSpider started!");
    }

}
