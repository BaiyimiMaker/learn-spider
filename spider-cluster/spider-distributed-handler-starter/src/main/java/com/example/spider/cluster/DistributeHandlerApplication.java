package com.example.spider.cluster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 分布式版本，本应用只处理爬虫结果
 * @author lym
 */
@EnableScheduling
@SpringBootApplication
public class DistributeHandlerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistributeHandlerApplication.class, args);
    }

}
