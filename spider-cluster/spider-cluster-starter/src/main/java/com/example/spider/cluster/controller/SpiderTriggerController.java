package com.example.spider.cluster.controller;

import cn.hutool.core.map.MapUtil;
import com.example.spider.common.starter.SpiderStarter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author lym
 */
@RestController
@RequestMapping("spider")
public class SpiderTriggerController {

    @Autowired(required = false)
    private Map<String, SpiderStarter> spiderStarterMap;

    @GetMapping("triggerAll")
    public String triggerAll(){
        if(MapUtil.isNotEmpty(spiderStarterMap)){
            spiderStarterMap.forEach((name, starter) -> starter.startSpiders());
            return "triggered all";
        }else {
            return "none SpiderStarter";
        }
    }

}
