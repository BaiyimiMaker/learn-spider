package com.example.spider.cluster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 集群版本，一个应用既能爬取，又能处理
 * @author lym
 */
@EnableScheduling
@SpringBootApplication
public class ClusterSpiderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClusterSpiderApplication.class, args);
    }

}
