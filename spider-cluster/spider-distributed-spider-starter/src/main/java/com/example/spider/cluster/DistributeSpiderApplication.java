package com.example.spider.cluster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 分布式版本，本应用只处理爬虫任务
 * @author lym
 */
@EnableScheduling
@SpringBootApplication
public class DistributeSpiderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DistributeSpiderApplication.class, args);
    }

}
